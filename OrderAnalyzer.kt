import java.math.BigDecimal
import java.time.DayOfWeek
import java.time.LocalDateTime

class OrderAnalyzer {
    data class Order(val orderId: Int, val creationData: LocalDateTime, val orderLines: List<OrderLine>)
    data class OrderLine(val productId: Int, val name: String, val quantity: Int, val unitPrice: BigDecimal)

    fun totalDailySales(orders: List<Order>): Map<DayOfWeek, Int> {
        val quantitiesPerDay = mutableMapOf<DayOfWeek, Int>()
        for (order in orders){
            for (orderLine in order.orderLines){
                val currentDayQuantity = quantitiesPerDay[order.creationData.dayOfWeek] ?: 0
                quantitiesPerDay[order.creationData.dayOfWeek] = currentDayQuantity + orderLine.quantity
            }
        }
        return quantitiesPerDay
    }
}

fun main() {
    val orderline1 = OrderAnalyzer.OrderLine(9876, "pencil", 3, unitPrice = BigDecimal(3.30))
    val orderline2 = OrderAnalyzer.OrderLine(977, "pen", 2, unitPrice = BigDecimal(4.00))
    val order1 = OrderAnalyzer.Order(988, LocalDateTime.of(2019, 11, 15, 8, 0), listOf(orderline1, orderline2))

    val orderline3 = OrderAnalyzer.OrderLine(9874, "eraser", 2, unitPrice = BigDecimal(2.30))
    val order2 = OrderAnalyzer.Order(988, LocalDateTime.of(2019, 11, 8, 8, 0), listOf(orderline3))

    val orderline4 = OrderAnalyzer.OrderLine(9773, "ruler", 2, unitPrice = BigDecimal(2.00))
    val order3 = OrderAnalyzer.Order(988, LocalDateTime.of(2019, 11, 16, 8, 0), listOf(orderline4))

    val orderlist = arrayListOf<OrderAnalyzer.Order>(order1, order2, order3)
    println(OrderAnalyzer().totalDailySales(orderlist))
}